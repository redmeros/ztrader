﻿using System;
using System.IO;
using NLog;

using Microsoft.Extensions.Configuration.CommandLine;
using Microsoft.Extensions.Configuration.EnvironmentVariables;
using Microsoft.Extensions.Configuration.Json;
using Microsoft.Extensions.Configuration;

namespace ZShared
{
    public static class ZConfig
    {
        private static IConfiguration _config = null;

        public static ILogger Logger = NLog.LogManager.GetCurrentClassLogger();

        public static IConfiguration GetConfig()
        {
            if (_config == null)
                _config = BuildConfig();
            return _config;
        }

        private static IConfiguration BuildConfig()
        {
            var configFileName = "config.json";

            var dir = new DirectoryInfo(Directory.GetCurrentDirectory()).FullName;
            ZConfig.Logger.Info(string.Format("Reading config from {0}/{1}", dir.ToString(), configFileName));
            var builder = new ConfigurationBuilder()
                .SetBasePath(dir)
                .AddJsonFile(configFileName);
            return builder.Build();
        }

    }
}
