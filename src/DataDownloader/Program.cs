﻿using System;
using System.IO;
using System.Globalization;
using NLog;
using Microsoft.Extensions.Configuration;
using ZShared;

namespace DataDownloader
{
    static class Program
    {
        public static IConfiguration Config { get; set; }
        public static ILogger Logger = NLog.LogManager.GetCurrentClassLogger();
        public static readonly string DateFormat = "yyyyMMdd";

        static void Main(string[] args)
        {
            Logger.Info("Uruchomiono ZDownloader");

            Config = ZShared.ZConfig.GetConfig();


            var downloaderConfig = Config.GetSection("downloader");
            var symbols = downloaderConfig.GetSection("symbols");
            foreach (var symbol in symbols.GetChildren())
            {
                try
                {
                    var splitter = new DateSplitter();
                    var symbolName = symbol["name"];
                    var fromDate = DateTime.ParseExact(symbol["fromDate"], DateFormat, CultureInfo.InvariantCulture);
                    var toDate = DateTime.ParseExact(symbol["toDate"], DateFormat, CultureInfo.InvariantCulture);
                    var granurality = Enum.Parse<Granurality>(symbol["granurality"]);

                    splitter.FromDate = fromDate;
                    splitter.ToDate = toDate;
                    splitter.Granurality = granurality;

                    Logger.Info("Poprawnie stworzono splittera");
                    var dates = splitter.SplitDates();
                    foreach (var splitted in splitter.SplitDates())
                    {
                        var d = new Downloader(symbolName, splitted.FromDate, granurality);
                        Logger.Trace(d.GetUrl());
                    }
                }
                catch (Exception ex)
                {
                    Logger.Warn(String.Format("Nie przetworzono: {0}", symbol["name"]));
                    Logger.Warn(ex.Message);
                    continue;
                }
            }
            Logger.Info("All symbols processed.");

            Logger.Info("Closing ZDownloader");
            LogManager.Shutdown();

            Console.ReadKey();
        }
    }
}
