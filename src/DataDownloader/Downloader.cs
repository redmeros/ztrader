﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;

using System.Runtime.Serialization.Json;

using NLog;

namespace DataDownloader
{
    public class Downloader
    {
        public string Symbol { get; set; } = "EUR_USD";
        public DateTime Date { get; set; }
        public Granurality Granurality { get; set; }
        protected Logger Logger = NLog.LogManager.GetCurrentClassLogger();

        public string Token { get; set; }
        public string AccountNo { get; set; }
        public string BaseUrl { get; set; }

        private void configure()
        {
            var configSection = ZShared.ZConfig.GetConfig().GetSection("oanda");
            Token = configSection["token"];
            BaseUrl = configSection["url"];
            AccountNo = configSection["account"];
        }

        public Downloader()
        {
            configure();
        }

        public Downloader(string symbol, DateTime date, Granurality granurality)
        {
            Symbol = symbol;
            Date = date;
            Granurality = Granurality;
            configure();
        }

        public Uri GetUrl()
        {
            return GetUrl(this.Symbol, this.Date, this.Granurality);
        }

        //var res = dl.GetUrl("EUR_USD", new DateTime(2018, 01, 01, 12, 00, 00), Granurality.S5);
        //"https://api-fxtrade.oanda.com/v3/instruments/USD_JPY/candles?count=10&price=A&from=2016-01-01T00%3A00%3A00.000000000Z&granularity=D
        public Uri GetUrl(string symbol, DateTime fromDate, Granurality g)
        {
            var u = new Uri(
                String.Format("{3}instruments/{0}/candles?count=1000&from={1}&granurality={2}",
                symbol,
                fromDate.ToRfc3339String(),
                g.ToString(),
                this.BaseUrl
                ));
            return u;
        }

        public string Download()
        {
            throw new NotImplementedException();
        }

    }
}
