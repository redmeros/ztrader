﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace DataDownloader
{
    public static class Extensions
    {
        public static string ToRfc3339String(this DateTime dateTime)
        {
            return dateTime.ToString("yyyy-MM-dd'T'HH:mm:ss", CultureInfo.InvariantCulture);
        }
    }
}
