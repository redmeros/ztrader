﻿using System;
using System.Linq;
using System.Collections.Generic;
using System.Text;


namespace DataDownloader
{
    public enum Granurality
    {
        S5 = 5,
        S10 = 10,
        S15 = 15,
        S30 = 30,
        M1 = 60,
        M2 = 120,
        M4 = 240,
        M5 = 300,
        M10 = 600,
        M15 = 900,
        M30 = 1800,
        H1 = 3600,
        H2 = 7200,
        H3 = 10800,
        H4 = 14400,
        H6 = 21600,
        H8 = 28800,
        H12 = 43200,
        D = 86400,
        W = 604800,
        M = 18144000
    }

    public class SplittedDate
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
    
    public class DateSplitter
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public Granurality Granurality { get; set; }

        public readonly int MaxCount = 1000;

        public List<SplittedDate> SplitDates(Granurality granurality = 0)
        {
            if (granurality == 0)
            {
                granurality = this.Granurality;
            }
            var result = new List<SplittedDate>();
            var currentDate = FromDate;
            while (currentDate < ToDate)
            {
                result.Add(
                    new SplittedDate()
                        {
                            FromDate = currentDate,
                            ToDate = currentDate.AddSeconds((int)granurality * MaxCount)
                        }
                    );
                currentDate = currentDate.AddSeconds((int)granurality * MaxCount);
                if (currentDate > ToDate)
                {
                    currentDate = ToDate;
                    result[result.Count - 1].ToDate = ToDate;
                }
            }
            return result;
        }
    }
}
