
using NUnit.Framework;
using System;
using System.Linq;
using DataDownloader;
using System.Collections.Generic;

namespace DataDownloader.Tests
{
    public class DateSplitterTests
    {
        [SetUp]
        public void Setup()
        {
        }

        /// <summary>
        /// Je�li ilo�� przedzia�ow jest mniejsza ni� 1000 (zmienna DateSplitter.MaxCount)
        /// Powinien zwr�ci� dok�adnie jeden wynik z przedzia�em zadanym od pocz�tku
        /// </summary>
        [Test]
        public void DateSplitingS5Test()
        {
            var splitter = new DateSplitter()
            {
                FromDate = new DateTime(2018, 01, 01, 00, 00, 00),
                ToDate = new DateTime(2018, 01, 01, 00, 01, 00),
                Granurality = Granurality.S5
            };

            var result = splitter.SplitDates(Granurality.S5);
            Assert.AreEqual(1, result.Count);

            Assert.AreEqual(splitter.FromDate, result[0].FromDate);

            Assert.AreEqual(splitter.ToDate, result[0].ToDate);
        }

        /// <summary>
        /// 10 to dok�adnie 24 * 60 * 60 = 864 00 sekund
        /// powinno to da� 24 * 60 * 60 / 5 = 172 80 �wieczek
        /// czyli powinni�my mie� 17,28 - czyli 18 przedzia�y
        /// </summary>
        [Test]
        public void DateSplittingOver1kWithBadSeconds()
        {
            var splitter = new DateSplitter()
            {
                FromDate = new DateTime(2018, 01, 01, 00, 00, 00),
                ToDate = new DateTime(2018, 01, 02, 00, 00, 01),
                Granurality = Granurality.S5
            };

            // jeden przedzia� to 1000 * 5s = 5000s
            var result = splitter.SplitDates(Granurality.S5);
            Assert.AreEqual(18, result.Count);

            //sprawdzam pierwszy przedzial
            var div1 = result.First();
            Assert.AreEqual(splitter.FromDate, div1.FromDate);
            Assert.AreEqual(splitter.FromDate.AddSeconds(1000 * (int)splitter.Granurality), div1.ToDate);

            // sprawdzam ostatni przedzial
            // jesli przedzial jest nierowny to zle to liczy ...
            var div2 = result.Last();

            // reszta z dzielenia
            var seconds = (splitter.ToDate - splitter.FromDate).TotalSeconds;

            // a co jesli seconds / splitter granurality sie nie podzieli przez siebie do liczby calkowitej?
            // wezmy pod uwag� taki przypadek 
            // ze bedzie 12 sekund  => do napisania test testu :P ?
            var r2 = seconds % (int)splitter.Granurality;
            var r = ((int)seconds / (int)splitter.Granurality) % 1000;

            Assert.AreEqual(splitter.ToDate.AddSeconds(-r * (int)splitter.Granurality - r2), div2.FromDate);
            Assert.AreEqual(splitter.ToDate, div2.ToDate);
        }
    }
}