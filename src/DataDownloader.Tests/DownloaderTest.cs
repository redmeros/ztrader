﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using DataDownloader;

namespace DataDownloader.Tests
{
    [TestFixture]
    public class DownloaderTests
    {

        [SetUp]
        public void setup()
        {
            ZShared.ZConfig.GetConfig();
        }

        [Test]
        public void GetUrlTest()
        {
            var dl = new Downloader();
            var res = dl.GetUrl("EUR_USD", new DateTime(2018, 01, 01, 12, 00, 00), Granurality.S5);

            Assert.AreEqual(
                new Uri("https://api-fxpractice.oanda.com/v3/instruments/EUR_USD/candles?count=1000&from=2018-01-01T12:00:00&granurality=S5"),
                res);
        }
    }
}
