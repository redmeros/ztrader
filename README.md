# ZTrader

`To jest projekt który ma działać trochę jak QuantConnect Lean, natomiast z innymi założeniami.`

## Założenia

1. Projekt ma dostarczyć strategię dla mnie, w związku z tym nie chcę być ograniczony bibliotekami dostępnymi online w Lean.

2. Głównym działaniem projektu jest backtesting i research w związku z tym używanie do tego platformy chmurowej z ograniczeniami jest bez sensu

3. Na koniec, Lean stał się tak skomplikowany, że jego konfiguracja i prace nad klientem desktopowym trwają w nieskończoność

## ROADMAP

1. Toolbox do ściągania i zapisywania danych - język dowolny - byle szybciej
2. System konfiguracji, taki aby ustawienia osobiste nie mogły
3. Shared, coś co pozwoli na niepowtarzanie kodu.
4. Szkielet na zasadzie eventów - docelowo projekt ma być responsywny (nie wiem jak to napisać) - wykonanie nie ma być liniowe :D
5. Projekt docelowo ma się uruchamiać na linuxie, tak aby można było kupić sobie taniego vps (patrz aruba i tam go uruchomić)
6. Projekt ma nie być skomplikowany, tak aby można go było używać modułowo lub żeby ktoś kto nie ma na celu ogarnianie projektu a tylko wykorzystanie go mógł w łatwy sposób go skonfigurować i zmodyfikować pod siebie
7. Projekt powinien być prowadzony w języku angielskim, aby jak najwięcej osób mogło z niego skorzystać i wytykać błędy.
8. Program główny powinien mieć czytelny i nieskazitelny log, tzn powinien logować wszystkie informacje, tak aby można było go w normalny sposób zaprezentować jakimś zewnętrznym oprogramowaniem.

## WYBÓR TECHNOLOGII

### Wymagania

1. Powinna być szybka, im mniej narzutów tym lepiej,
2. Im więcej problemów wyjdzie podczas budowania tym lepiej - stabilność kodu produkcyjnego
3. Technologia powinna umożliwić interakcje sieciowe,
4. Im mniej zbędnego kodu tym lepiej,
5. Narzędzia pozwalające na w miarę łatwą interakcję ze środowiskiem web,
6. Statyczne typowanie (nie chcę się domyślać co autor miał na myśli pisząc bibliotekę),

### Możliwe wybory

- Python
  - Brak statycznego typowania, daje swobodę ale i trudności
  - Uruchamia się wszędzie i ma pip'a !!
- Qt/C++
  - 